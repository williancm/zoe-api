const express = require('express');
const routes = express.Router();

// Default route
routes.get('/status', (req,res) => res.json({
    "message" : "API is working",
}));

// USER ROUTES
const UserController = require('./controllers/UserController');

routes.post('/users/message',UserController.sendMessage);



module.exports = routes;