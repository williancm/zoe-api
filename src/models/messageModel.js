class Message{
    constructor(username, payload, timestamp){
        this.username = username;
        this.payload = payload;
        this.timestamp = timestamp;
    }
}


module.exports = Message;